from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from users.forms import AddGroupForm
from .models import User, Group

class GroupAdmin(admin.ModelAdmin):
    form = AddGroupForm

admin.site.register(User, UserAdmin)
admin.site.register(Group, GroupAdmin)