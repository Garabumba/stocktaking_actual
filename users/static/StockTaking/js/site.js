var url = window.location.href;
var sections = url.split('/');
let lastSection = sections.pop() || sections.pop();
var removedElements = [];

var canvas = new fabric.Canvas('canvas');

$.ajax({
    type: 'GET',
    //contentType: 'application/json',
    //url: `/audience/get_state/${lastSection}/`,
    url: `/api/get_audience_state/${lastSection}/`,
    success: function (data) {
        if (data.state != null) {
            canvas.loadFromJSON(data.state, function() {
                canvas.renderAll();
                
                if (data.photo != null)
                    if (canvas.backgroundImage == null) {
                        //var imgElement = document.createElement('img');
                        var imgElement = new Image();
                        imgElement.src = data.photo;//'/media/home/bg.png';

                        // canvas.setBackgroundImage(imgElement.src, canvas.renderAll.bind(canvas), {
                        //     scaleX: 300.0 / imgElement.width,
                        //     scaleY: 150.0 / imgElement.height,
                        // });

                        imgElement.onload = function() {
                            canvas.setBackgroundImage(imgElement.src, canvas.renderAll.bind(canvas), {
                                scaleX: 300.0 / imgElement.width,
                            scaleY: 150.0 / imgElement.height,
                            });
                        };
                    }
                
                // imgElement.onload = function() {
                //     // Создаем новый объект изображения Fabric.js
                //     var imgInstance = new fabric.Image(imgElement, {
                //         // Устанавливаем параметры изображения
                        
                //         left: 0,
                //         top: 0,
                //         selectable: false  // Не разрешаем выбор изображения
                //     });
                    
                //     // Добавляем изображение на canvas
                //     canvas.add(imgInstance);
                // };
            },
            function(o, figure){
                //console.log(figure.src)
                figureController(figure, false);
                updateFigures(figure.figureId);
            })
        }
        else {
            
        }
    }
});

var switchInput = document.getElementById('flexSwitchCheckDefault');

if (switchInput.checked) {
    $(".tools").css("display", "block");
    $(".t").css("display", "grid");
    const contentDiv = document.querySelector('#main');
const resizeObserver = new ResizeObserver(() => {
    resizeCanvas();
});

// Наблюдать за изменениями размера элемента
resizeObserver.observe(contentDiv);
}
else {
    $(".tools").css("display", "none");
    $(".t").css("display", "block");
    const contentDiv = document.querySelector('#main');
const resizeObserver = new ResizeObserver(() => {
    resizeCanvas();
});

// Наблюдать за изменениями размера элемента
resizeObserver.observe(contentDiv);
}

    // Добавляем обработчик события изменения состояния чекбокса
switchInput.addEventListener('change', function() {
    // Проверяем текущее состояние чекбокса
    checked = this.checked;

    if (checked) {
        $(".tools").css("display", "block");
        $(".t").css("display", "grid");
        const contentDiv = document.querySelector('#main');
const resizeObserver = new ResizeObserver(() => {
    resizeCanvas();
});

// Наблюдать за изменениями размера элемента
resizeObserver.observe(contentDiv);
    }
    else {
        $(".tools").css("display", "none");
        $(".t").css("display", "block");
        const contentDiv = document.querySelector('#main');
const resizeObserver = new ResizeObserver(() => {
    resizeCanvas();
});

// Наблюдать за изменениями размера элемента
resizeObserver.observe(contentDiv);
    }

    canvas.discardActiveObject();
    canvas.requestRenderAll();
    canvas.forEachObject(function(obj) {
            //obj.selectable = true;
        obj.__eventListeners["mousedown"] = [];
        figureController(obj, checked);
    });
    // if (this.checked) {
    //     console.log('Переключатель включен');
    //     canvas.forEachObject(function(obj) {
    //         //obj.selectable = true;
    //         figureController(obj, this.checked);
    //     });
    // } else {
    //     console.log('Переключатель выключен');
    // }
});

function resizeCanvas() {
    const outerCanvasContainer = document.getElementById('stage-parent');

    const ratio          = canvas.getWidth() / canvas.getHeight();
    const containerWidth = outerCanvasContainer.offsetWidth - 30;
    const scale          = containerWidth / canvas.getWidth();
    const zoom           = canvas.getZoom() * scale;

    canvas.setDimensions({width: containerWidth, height: containerWidth / ratio});
    canvas.setViewportTransform([zoom, 0, 0, zoom, 0, 0]);
}

resizeCanvas();

window.addEventListener('resize', resizeCanvas);
//$('#sdsa').addEventListener('resize', resizeCanvas);

const contentDiv = document.querySelector('#main');
const resizeObserver = new ResizeObserver(() => {
    resizeCanvas();
});

// Наблюдать за изменениями размера элемента
resizeObserver.observe(contentDiv);

document.getElementById('saveState').addEventListener(
    'click',
    function () {
        console.log(canvas);
        var json = JSON.stringify(canvas);
        audienceId = $("#saveState").attr('data-id');
        var csrftoken = getCookie('csrftoken');

        $.ajax({
            url: `/api/change_audience_state/${audienceId}/`,
            type: 'PUT',
            beforeSend: function(xhr) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            },
            contentType: 'application/json',
            data: json,
            dataType: 'json',
            success: function (data) {
                console.log('Данные успешно отправлены на сервер');
            },
            error: function (xhr, status, error) {
                console.error('Ошибка при отправке данных на сервер:', error);
            }
        })
        
        console.log(json);
    },
    false
);

function sendNewRequest()
{
    const title = $('#titleInput').val();
    const message = $('#messageInput').val();
    const inventoryNumber = $('#sendButton').attr('data-id');

    const requestObject = {
        request: {
            title: title,
            message: message,
            inventory_number: inventoryNumber,
            users: []
        }
    };
    
    const json = JSON.stringify(requestObject);
    //console.log(json);
    var csrftoken = getCookie('csrftoken');
    $.ajax({
        url: '/api/create_request/',
        type: 'POST',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        },
        contentType: 'application/json',
        data: json,
        dataType: 'json',
        success: function (data) {
            console.log('Данные успешно отправлены на сервер');
        },
        error: function (xhr, status, error) {
            console.error('Ошибка при отправке данных на сервер:', error);
        }
    })
}

function testClick()
{
    const inventoryNumber = $('#createRequest').attr('data-id');
    const insert_elements = `
    <div class="form-group">
        <label for="titleInput">Заголовок</label>
        <input type="text" class="form-control" id="titleInput" maxlength="50">
    </div>
    <div class="form-group">
        <label for="messageInput">Сообщение</label>
        <textarea class="form-control" id="messageInput" rows="4" maxlength="255"></textarea>
    </div>
    <button type="button" class="btn btn-primary" id="sendButton" onclick="sendNewRequest()" data-id="${inventoryNumber}">Отправить</button>
    <button type="button" class="btn btn-secondary" id="cancelButton">Отменить</button>
`;

    $('#exampleModalLabel').html(`Создание обращения`);

    $('.modal-body').html(insert_elements);

    // console.log('click');
    // const requestObject = {
    //     request: {
    //         title: "test title",
    //         message: "test message",
    //         inventory_number: $('#createRequest').attr('data-id')
    //     }
    // };
    
    // const json = JSON.stringify(requestObject);
    // console.log(json);

    // $.ajax({
    //     url: '/create_request/',
    //     type: 'POST',
    //     contentType: 'application/json',
    //     data: json,
    //     dataType: 'json',
    //     success: function (data) {
    //         console.log('Данные успешно отправлены на сервер');
    //     },
    //     error: function (xhr, status, error) {
    //         console.error('Ошибка при отправке данных на сервер:', error);
    //     }
    // })

    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    // console.log(json);
}

// $("#createRequest").on( "click", function() {
//     console.log('click');
//     const requestObject = {
//         request: {
//             title: "test title",
//             message: "test message",
//             computer: $(this).attr('data-id')
//         }
//     };
    
//     const json = JSON.stringify(requestObject);
//     console.log(json);

//     // $.ajax({
//     //     url: '/audience/change_state/',
//     //     type: 'PUT',
//     //     contentType: 'application/json',
//     //     data: json,
//     //     dataType: 'json',
//     //     success: function (data) {
//     //         console.log('Данные успешно отправлены на сервер');
//     //     },
//     //     error: function (xhr, status, error) {
//     //         console.error('Ошибка при отправке данных на сервер:', error);
//     //     }
//     // })
    
//     // console.log(json);
// });

function figureController(figure, isEdit = true)
{
    //console.log("is edit:" + isEdit);
    figure.on('mouseover', function() {
        canvas.hoverCursor = 'pointer';
        canvas.renderAll();
    });
    if (!isEdit)
    {
        //console.log('sss');
        figure.selectable = false;
        figure.on('mousedown', function(e) {
            //console.log(e.target.figureId);
            // var myModal = document.getElementById('myModal')
            // var myInput = document.getElementById('exampleModal')

            // //myModal.addEventListener('shown.bs.modal', function () {
            // myInput.focus()
            //$('#exampleModal').attr('id', figure.figureId);
            //console.log(figure.figureId);
            var url = "";
            url = `/api/get_technique_info/?inventory_number=${figure.figureId}`;
            // if (figure.techniqueId == 1)
            //     url = `/get_technique_info/?inventory_number=${figure.figureId}&technique_type=1`;
            // else if (figure.techniqueId == 2)
            //     url = `/get_technique_info/?inventory_number=${figure.figureId}&technique_type=2`;
            // else if (figure.techniqueId == 3)
            //     url = `/get_technique_info/?inventory_number=${figure.figureId}&technique_type=3`;
            $.ajax({
                type: 'GET',
                //contentType: 'application/json',
                url: url,//`/get_computer_info/?computer_id=${figure.figureId}`,
                success: function (data) {
                    if (data != null) {
                        //console.log(data);
                        
                        $('#exampleModalLabel').html(figure.figureId);
                        if (figure.techniqueId == 1) {
                            insert_elements = `<h1>Компьютер</h1><div><b>Имя компьютера: </b>${data.name}</div>`;
                            insert_elements += `<div><b>Инвентарный номер компьютера: </b>${data.inventory_number}</div>`;
                            insert_elements += `<h1>Материнская плата</h1><div>${data.motherboard.name}</div>`;
                            insert_elements += `<button class="createReuqest btn btn-success" id="createRequest", data-id=${figure.figureId} onclick="testClick()">Создать обращение</button>`;
                        }
                        else if (figure.techniqueId == 2) {
                            insert_elements = `<h1>Проектор</h1><div><b>Имя проектора: </b>${data.name}</div>`;
                            insert_elements += `<div><b>Инвентарный номер проектора: </b>${data.inventory_number}</div>`;
                            insert_elements += `<button class="createReuqest btn btn-success" id="createRequest", data-id=${figure.figureId} onclick="testClick()">Создать обращение</button>`;
                        }
                        else if (figure.techniqueId == 3) {
                            insert_elements = `<h1>Принтер</h1><div><b>Имя принтера: </b>${data.name}</div>`;
                            insert_elements += `<div><b>Инвентарный номер принтера: </b>${data.inventory_number}</div>`;
                            insert_elements += `<button class="createReuqest btn btn-success" id="createRequest", data-id=${figure.figureId} onclick="testClick()">Создать обращение</button>`;
                        }
                        $('.modal-body').html(insert_elements);
                        //$('.modal-body').html(``);
                    }
                    else {
                        console.log("Беда");
                    }
                }
            });
            $('#exampleModal').modal('show');
            //})
            
            console.log($('#createRequest'));
            console.log("hello");
        });
    }
    else
    {
        figure.selectable = true;
    }
}

function createNewFigure(id, typeId) {
    console.log(id);
    console.log(typeId);
    var statusId = 0;
    var url = "";
    //console.log(typeId);
    url = `/api/get_technique_info/?inventory_number=${id}`;
    // if (typeId == 1)
    //     url = `/get_technique_info/?inventory_number=${id}&technique_type=1`;
    //     //url = `/get_computer_info/?computer_id=${id}`;
    // else if (typeId == 2)
    //     url = `/get_technique_info/?inventory_number=${id}&technique_type=2`;
    // else if (typeId == 3)
    //     url = `/get_printer_info/?inventory_number=${id}`;
    $.ajax({
        type: 'GET',
        //contentType: 'application/json',
        url: url,//`/get_computer_info/?computer_id=${id}`,
        success: function (data) {
            if (data != null) {
                console.log(data);
                statusId = data.status.id;
                techniqueId = data.technique_type_id;
                tt(id, statusId, techniqueId);
            }
            else {
                console.log("Беда");
            }
        }
    });
    // else if (typeId == 3)
    // $.ajax({
    //     type: 'GET',
    //     //contentType: 'application/json',
    //     url: `/get_printer_info/?inventory_number=${id}`,
    //     success: function (data) {
    //         if (data != null) {
    //             console.log(data);
    //             //statusId = data.status.id;
    //             statusId = 1;
    //             tt(id, statusId, );
    //         }
    //         else {
    //             console.log("Беда");
    //         }
    //     }
    // });
}

function tt(id, statusId, techniqueId) {
    var image = document.getElementById('my-image');
    console.log(techniqueId);
    console.log(statusId);

    if (techniqueId == 1 && statusId == 1)
        image = document.getElementById('my-image');
    else if (techniqueId == 1 && statusId == 2)
        image = document.getElementById('my-image2');
    else if (techniqueId == 2 && statusId == 1)
        image = document.getElementById('my-image3');
    else if (techniqueId == 2 && statusId == 2)
        image = document.getElementById('my-image4');
    else if (techniqueId == 3 && statusId == 1)
        image = document.getElementById('my-image5');
    else if (techniqueId == 3 && statusId == 2)
        image = document.getElementById('my-image6');
    else if (techniqueId == 4 && statusId == 1)
        image = document.getElementById('my-image7');
    else if (techniqueId == 4 && statusId == 2)
        image = document.getElementById('my-image8');
    else if (techniqueId == 5 && statusId == 1)
        image = document.getElementById('my-image9');
    else if (techniqueId == 5 && statusId == 2)
        image = document.getElementById('my-image10');

    var figure = new fabric.Image(image, {
        figureId: id,
        statusId: statusId,
        techniqueId: techniqueId
    });
    figure.scale(0.04);

    canvas.add(figure);
    //console.log(canvas._objects);
    updateFigures(id);
    figureController(figure);
}

$('.newComputer').click(function(){
    createNewFigure(this.id, $(this).attr('data-id'));
});

function updateFigures(id) {
    removedElements.push($(`#${id}`).parent());
    //console.log(id);
    //console.log(removedElements[0]);
    $(`#${id}`).parent().remove();
    
    canvas.forEachObject(function(obj) {
            //obj.selectable = true;
        //obj.__eventListeners["mousedown"] = [];
        //figureController(obj, checked);
        console.log(obj);
    });
}

document.addEventListener('keydown', function (e) {
    if (e.keyCode === 46) {
        var ids = [];
        var array = [];
        
        if (canvas.getActiveObject()._objects != undefined)
            for (let i = 0; i < canvas.getActiveObject()._objects.length; i++)
            {
                ids.push(canvas.getActiveObject()._objects[i].figureId);
                canvas.remove(canvas.getActiveObject()._objects[i]);
            }
        else
        {
            ids.push(canvas.getActiveObject().figureId);
            canvas.remove(canvas.getActiveObject());
        }
        
        ids.forEach((id) => {
            removedElements.forEach((removedElement) => {
                if ($($(removedElement).children()[0]).attr('id') == id) {
                    array.push(removedElement);
                }
            })
        })
        
        array.forEach((el) => {
            var index = removedElements.indexOf(el);
            let id = $($(el).children()[0]).attr('id');
            let dataId = $($(el).children()[0]).attr('data-id');
            $(`#tech${dataId}`).append(removedElements[index]);
            removedElements.splice(index, 1);
            $(`#${id}`).click(function(){
                createNewFigure(id, $(this).attr('data-id'));
            });
        })
    }
});

// var switchInput = document.getElementById('flexSwitchCheckDefault');

//   // Проверяем его текущее состояние
//   if (switchInput.checked) {
//     console.log('Переключатель включен');
//     // Ваш код, если переключатель включен
//   } else {
//     console.log('Переключатель выключен');
//     // Ваш код, если переключатель выключен
//   }

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}